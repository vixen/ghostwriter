from django.conf import settings
from django.urls import include, path
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import TemplateView
from .views import scene, res

urlpatterns = [
    path('', scene, name='scene'),
    path('res', res, name='scene_res')
]
