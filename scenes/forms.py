from django import forms
from django.forms import Textarea, CharField
from django.utils.translation import gettext_lazy as _

from .models import Scene, Event,Fixed_point


class SceneForm(forms.ModelForm):

    class Meta:
        model = Scene
#
#
        fields = ('scene_name', 'scene_summary', 'heroes',
                  'scene_date', 'scene_place', 'focal',
                  'atmosphere', 'idea', 'hints',

                  'goal', 'conflict', 'disaster',

                  'reaction', 'dilemma', 'solution', 'hook',
                  'events',
                  'fixed_point',
                  )

        # labels = {
        #     'scene_name': _('Название сцены'),
# scene_summary = models.CharField(max_length=200)  # Краткое содержание
# heroes = models.CharField(max_length=100)  # Персонажи
# scene_date = models.DateField()  # Дата
# scene_place = models.CharField(max_length=50)  # Место
# focal = models.CharField(max_length=50)  # Фокал
# atmosphere  # Атмосфера
# idea  # Идея
# hints  # Намеки
#
# goal  # Цель
# conflict  # Конфликт
# disaster  # Катастрофа
#
#
# reaction  # Реакция
# dilemma  # Дилемма
# solution  # Решение
# hook  # Крючек
# events  # События
#
#
#     События
# }
#         #
#         # error_messages = {
#         #     'why_important': {
#         #         'max_length': _("This writer's name is too long."),
#         #     },
#         # }

 # widgets = {
#         #     'why_important': Textarea(attrs={'cols': 20, 'rows': 5}),
#         # }
