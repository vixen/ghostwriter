from datetime import timezone

from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.template import loader
from .models import Scene
from .forms import SceneForm


def res(request):
    scenes = Scene.objects.all()
    return render(request, 'scenes/scene.html', {'scenes': scenes})


def scene(request):
    if request.method == "POST":
        form = SceneForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect('res')
    else:
        form = Scene()
    return render(request, 'scenes/index.html', {'form': form})
