from django.db import models



class Event(models.Model):
    events = models.CharField(max_length=200)

    def __str__(self):              # __unicode__ on Python 2
        return "%s" % (self.events)


class Fixed_point(models.Model):
    fixed_points = models.CharField(max_length=200)

    def __str__(self):              # __unicode__ on Python 2
        return "%s" % (self.fixed_points)


class Scene(models.Model):
    scene_name = models.CharField(max_length=50) # Название сцены
    scene_summary = models.CharField(max_length=200) # Краткое содержание
    heroes = models.CharField(max_length=100) # Персонажи
    scene_date = models.DateField() # Дата
    scene_place = models.CharField(max_length=50) # Место
    focal = models.CharField(max_length=50) # Фокал
    atmosphere = models.CharField(max_length=50) # Атмосфера
    idea = models.CharField(max_length=50) # Идея
    hints = models.CharField(max_length=50) # Намеки

    # Причина

    goal = models.CharField(max_length=50) # Цель
    conflict = models.CharField(max_length=50) # Конфликт
    disaster = models.CharField(max_length=50) # Катастрофа

    # Следствие

    reaction = models.CharField(max_length=50) # Реакция
    dilemma = models.CharField(max_length=50) # Дилемма
    solution = models.CharField(max_length=50) # Решение
    hook = models.CharField(max_length=50) # Крючек

   # События

    events = models.ForeignKey(Event, on_delete=models.CASCADE) # События
    fixed_point = models.ForeignKey(Fixed_point, on_delete=models.CASCADE)

    def __str__(self):              # __unicode__ on Python 2
        return "%s" % (self.scene_name)


class Scene_term(models.Model):
    scene_term = models.CharField(max_length=50)
    mean = models.CharField(max_length=512)

    def __str__(self):              # __unicode__ on Python 2
        return "%s" % (self.scene_term)

