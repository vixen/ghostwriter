from django.contrib import admin
from scenes.models import Scene, Event, Fixed_point, Scene_term

admin.site.register(Scene)
admin.site.register(Event)
admin.site.register(Fixed_point)
admin.site.register(Scene_term)
