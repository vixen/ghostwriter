from datetime import timezone

from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.template import loader
from .models import MainHero
from .forms import HeroForm

def heroes1(request):
    heroes = MainHero.objects.all()
    template = loader.get_template('heroes/index.html')
    context = {
        'heroes': heroes,
    }

    return HttpResponse(template.render(context, request))

def res(request):

    # История зарождается там, где first_name first_appear
    heroes = MainHero.objects.all()
    return render(request, 'heroes/result.html', {'heroes': heroes})

def heroes(request):
    if request.method == "POST":
        form = HeroForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect('res')
    else:
        form = HeroForm()
    return render(request, 'heroes/index.html', {'form': form})

