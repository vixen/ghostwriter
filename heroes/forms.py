from django import forms
from django.forms import Textarea, CharField
from django.utils.translation import gettext_lazy as _

from .models import MainHero

class HeroForm(forms.ModelForm):

    class Meta:
        model = MainHero

        fields = ('first_name', 'last_name', 'middle_name',
                  # 'date_of_birth',
                  'first_appear',
                  # 'why_important',
                  # 'loosing_goal',
                  # 'strong_enought_to_solve_the_problem',
                  )

        # widgets = {
        #     'why_important': Textarea(attrs={'cols': 20, 'rows': 5}),
        # }

        labels = {

             'first_name': _('Имя'),
             'last_name':_('Фамилия'),
             'middle_name':_('Отчество'),
        #      'date_of_birth':_('дата рождения в формате 2018-11-28'),
        #      'gender':_('пол'),
             'first_appear':_('Как он появляется в произведении?'),
        #      'why_important': _('Почему судьба гг должна заботить читателя?'),
        #      'loosing_goal':_('Что важного герой теряет или может потерять?'),
        #      'strong_enought_to_solve_the_problem':_('Сумеет ли герой решить проблему?'),
        }
        #
        # error_messages = {
        #     'why_important': {
        #         'max_length': _("This writer's name is too long."),
        #     },
        # }
